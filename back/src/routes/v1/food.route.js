const express = require('express');
const validate = require('../../middlewares/validate');
const foodValidation = require('../../validations/food.validation');
const foodController = require('../../controllers/food.controller');
const auth = require('../../middlewares/auth');

const router = express.Router();

router
  .get('/', auth(), foodController.getFoods)
  .post('/', auth(), validate(foodValidation.createFood), foodController.createFood)
  .get('/calorie-limit', auth(), foodController.getFoodsCaloriesLimit);

router.patch('/:foodId', auth(), validate(foodValidation.updateFood), foodController.updateFood);
router.delete('/:foodId', auth(), validate(foodValidation.deleteFood), foodController.deleteFood);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Foods
 *   description: Food management and retrieval
 */

/**
 * @swagger
 * /foods:
 *   post:
 *     summary: Create a Food
 *     tags: [Foods]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - date
 *               - calorie
 *               - userId
 *             properties:
 *               name:
 *                 type: string
 *               date:
 *                 type: date
 *                 format: date
 *               calorie:
 *                 type: number
 *               userId:
 *                  type: number
 *             example:
 *               name: banana
 *               date: Sat Aug 20 2022 20:00:23 GMT+0100 (UTC+01:00)
 *               calorie: 1000
 *               userId: 1
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Food'
 *
 *   get:
 *     summary: Get all foods
 *     tags: [Foods]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Food'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /foods/calorie-limit:
 *   get:
 *     summary: Get all calories by day
 *     tags: [Foods]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Food'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /foods/{id}:
 *   patch:
 *     summary: Update a food
 *     tags: [Foods]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Food id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               date:
 *                 type: date
 *                 format: date
 *               calorie:
 *                 type: number
 *               userId:
 *                  type: number
 *             example:
 *               name: banana
 *               date: Sat Aug 20 2022 20:00:23 GMT+0100 (UTC+01:00)
 *               calorie: 1000
 *               userId: 1
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Food'
 *       "400":
 *         $ref: '#/components/responses/DuplicateEmail'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a Food
 *     tags: [Foods]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Food id
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */
