const express = require('express');
const validate = require('../../middlewares/validate');
const mealValidation = require('../../validations/meal.validation');
const mealController = require('../../controllers/meal.controller');

const router = express.Router();

router.get('/', mealController.getMeals).post('/', validate(mealValidation.createMeal), mealController.createMeal);

router.patch('/:mealId', validate(mealValidation.updateMeal), mealController.updateMeal);
router.delete('/:mealId', validate(mealValidation.deleteMeal), mealController.deleteMeal);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Meals
 *   description: Meal management and retrieval
 */

/**
 * @swagger
 * /meals:
 *   post:
 *     summary: Create a Meal
 *     tags: [Meals]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - limit
 *             properties:
 *               name:
 *                 type: string
 *               limit:
 *                 type: number
 *             example:
 *               name: breakfast
 *               limit: 5
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Meal'
 *
 *   get:
 *     summary: Get all meals
 *     tags: [Meals]
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Meal'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /meals/{id}:
 *   patch:
 *     summary: Update a meal
 *     tags: [Meals]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Meal id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               limit:
 *                 type: number
 *             example:
 *               name: breakfast
 *               limit: 5
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Meal'
 *       "400":
 *         $ref: '#/components/responses/DuplicateEmail'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a Meal
 *     tags: [Meals]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Meal id
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */
