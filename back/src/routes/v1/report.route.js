const express = require('express');
const reportController = require('../../controllers/report.controller');
const auth = require('../../middlewares/auth');

const router = express.Router();

router.get('/', auth('reporting'), reportController.getReports);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Reports
 *   description: Admin reports & Stats
 */

/**
 * @swagger
 * /reports:
 *   get:
 *     summary: Get Reports
 *     tags: [Reports]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Food'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */
