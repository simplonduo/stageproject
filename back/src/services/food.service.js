const httpStatus = require('http-status');
const { Food } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a food
 * @param {Object} foodBody
 * @returns {Promise<Food>}
 */
const createFood = async (foodBody) => {
  return Food.create(foodBody);
};

/**
 * Get All for foods
 * @param {Boolean} allUser
 * @returns {Promise<Foods>}
 */
const getFoods = async (allUser, user, foodParams) => {
  return Food.aggregate(
    [
      {
        $lookup: {
          from: 'meals',
          let: { meal: '$meal' },
          pipeline: [{ $match: { $expr: { $eq: ['$$meal', '$_id'] } } }, { $project: { name: 1, date: 1, calorie: 1 } }],
          as: 'meal',
        },
      },
      { $unwind: '$meal' },
      {
        $lookup: {
          from: 'users',
          let: { userId: '$userId' },
          pipeline: [{ $match: { $expr: { $eq: ['$$userId', '$_id'] } } }, { $project: { name: 1, date: 1, calorie: 1, meal: 1 } }],
          as: 'user',
        },
      },
      { $unwind: '$user' },
      { $sort: { createdAt: -1 } },
      { $match: allUser ? {} : { userId: user._id } },
      { $match: foodParams?.from ? { date: { $gt: new Date(foodParams.from) } } : {} },
      { $match: foodParams?.to ? { date: { $lt: new Date(foodParams.to) } } : {} },
      {
        $project: {
          date: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
          name: 1,
          calorie: 1,
          meal: 1,
          user: 1,
        }
      },
    ]
  )
};

/**
 * Get food by id
 * @param {ObjectId} id
 * @returns {Promise<Food>}
 */
const getFoodById = async (id) => {
  return Food.findById(id);
};

/**
 * Update Food by id
 * @param {ObjectId} foodId
 * @param {Object} updateBody
 * @returns {Promise<Food>}
 */
const updateFoodById = async (foodId, updateBody) => {
  const food = await getFoodById(foodId);
  if (!food) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Food not found');
  }
  Object.assign(food, updateBody);
  await food.save();
  return food;
};

/**
 * Delete food by id
 * @param {ObjectId} foodId
 * @returns {Promise<Food>}
 */
const deleteFoodById = async (foodId) => {
  const food = await getFoodById(foodId);
  if (!food) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Food not found');
  }
  await food.remove();
  return food;
};

const DEFAULT_CALORIE_LIMIT = 2100;
/**
 * Get food calorie limit by day
 * @param {Object} User
 * @returns {Promise<>}
 */
const getFoodsCaloriesLimit = async (user) => {
  return Food.aggregate([
    { $match: { userId: user._id } },
    { $group: { _id: { $dateToString: { format: '%Y-%m-%d', date: '$date' } }, calorie: { $sum: '$calorie' } } },
    { $sort: { _id: -1 } },
    { $project: { _id: 0, date: '$_id', calorie: 1, isWarn: { $gte: ['$calorie', user.limit || DEFAULT_CALORIE_LIMIT] } } },
  ]);
};

module.exports = {
  createFood,
  getFoods,
  updateFoodById,
  deleteFoodById,
  getFoodsCaloriesLimit,
};
