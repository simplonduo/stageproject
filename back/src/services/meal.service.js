const httpStatus = require('http-status');
const { Meal } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a meal
 * @param {Object} mealBody
 * @returns {Promise<Meal>}
 */
const createMeal = async (mealBody) => {
  return Meal.create(mealBody);
};

/**
 * Get All for meals
 * @returns {Promise<Meals>}
 */
const getMeals = async () => {
  return Meal.find();
};

/**
 * Get meal by id
 * @param {ObjectId} id
 * @returns {Promise<Meal>}
 */
const getMealById = async (id) => {
  return Meal.findById(id);
};

/**
 * Update Meal by id
 * @param {ObjectId} mealId
 * @param {Object} updateBody
 * @returns {Promise<Meal>}
 */
const updateMealById = async (mealId, updateBody) => {
  const meal = await getMealById(mealId);
  if (!meal) {
    throw new ApiError(httpStatus.NOT_FOUND, 'meal not found');
  }
  Object.assign(meal, updateBody);
  await meal.save();
  return meal;
};

/**
 * Delete meal by id
 * @param {ObjectId} mealId
 * @returns {Promise<Meal>}
 */
const deleteMealById = async (mealId) => {
  const meal = await getMealById(mealId);
  if (!meal) {
    throw new ApiError(httpStatus.NOT_FOUND, 'meal not found');
  }
  await meal.remove();
  return meal;
};

module.exports = {
  createMeal,
  getMeals,
  updateMealById,
  deleteMealById,
  getMealById,
};
