const { Food } = require('../models');

const getDateLimit = (limit) => {
  if (!limit) return;
  const d = new Date();
  d.setDate(d.getDate() - limit);
  return d;
};

/**
 * Get Number of foods by Date range
 * @returns {Promise<>}
 */
const getFoodsNumberPerWeek = async (gt, lt) => {
  const matchQuery = { $gt: getDateLimit(gt) };
  if (lt) matchQuery.$lt = getDateLimit(lt);
  return Food.countDocuments({ createdAt: matchQuery });
};

/**
 * Get Calorie Average Per User
 * @returns {Promise<>}
 */
const getCalorieAveragePerUser = async () => {
  return Food.aggregate([
    {
      $lookup: {
        from: 'users',
        let: { userId: '$userId' },
        pipeline: [{ $match: { $expr: { $eq: ['$$userId', '$_id'] } } }, { $project: { name: 1, email: 1 } }],
        as: 'user',
      },
    },
    { $unwind: '$user' },
    { $match : { createdAt: {$gt: getDateLimit(7)} }},
    { $group: { _id: '$user', numberCalorie: { $sum: '$calorie' }, numberEntry: { $sum: 1 } } },
    { $project: { _id: 0, user: '$_id', numberCalorie: 1, numberEntry: 1 } },
  ]);
};

module.exports = {
  getFoodsNumberPerWeek,
  getCalorieAveragePerUser,
};
