module.exports.authService = require('./auth.service');
module.exports.emailService = require('./email.service');
module.exports.tokenService = require('./token.service');
module.exports.userService = require('./user.service');
module.exports.foodService = require('./food.service');
module.exports.mealService = require('./meal.service');
module.exports.reportService = require('./report.service');
