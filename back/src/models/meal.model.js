const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const mealSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  limit: {
    type: Number,
    required: true,
  },
});

// add plugin that converts mongoose to json
mealSchema.plugin(toJSON);

/**
 * @typedef Meal
 */
const Meal = mongoose.model('Meal', mealSchema);

module.exports = Meal;
