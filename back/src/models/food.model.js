const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const foodSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    date: {
      type: Date,
      required: true,
    },
    calorie: {
      type: Number,
      required: true,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    meal: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Meal',
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
foodSchema.plugin(toJSON);

/**
 * @typedef Food
 */
const Food = mongoose.model('Food', foodSchema);

module.exports = Food;
