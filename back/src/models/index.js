module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.Food = require('./food.model');
module.exports.Meal = require('./meal.model');
