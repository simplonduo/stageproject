const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { foodService, mealService } = require('../services');
const ApiError = require('../utils/ApiError');
const { isAdmin } = require('../config/roles');

const createFood = catchAsync(async (req, res) => {
  const foodsUser = await foodService.getFoods(false, req.user, req.query);
  const meal = await mealService.getMealById(req.body.meal);
  if (!meal) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Meal not found');
  }
  
  if( foodsUser.filter(i => i.meal._id.toString() === meal._id.toString()).length >= meal.limit){
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'Meal limit is reached');
  }
  const food = await foodService.createFood({...req.body, userId: req.user._id});
  res.status(httpStatus.CREATED).send(food);
});

const getFoods = catchAsync(async (req, res) => {
  const result = await foodService.getFoods(isAdmin(req.user.role), req.user, req.query);
  res.send(result);
});

const updateFood = catchAsync(async (req, res) => {
  const food = await foodService.updateFoodById(req.params.foodId, req.body);
  res.send(food);
});

const deleteFood = catchAsync(async (req, res) => {
  await foodService.deleteFoodById(req.params.foodId);
  res.status(httpStatus.NO_CONTENT).send();
});

const getFoodsCaloriesLimit = catchAsync(async (req, res) => {
  const result = await foodService.getFoodsCaloriesLimit(req.user);
  res.send(result);
});

module.exports = {
  createFood,
  getFoods,
  updateFood,
  deleteFood,
  getFoodsCaloriesLimit,
};
