const catchAsync = require('../utils/catchAsync');
const { reportService, foodService } = require('../services');
const { isAdmin } = require('../config/roles');

const getReports = catchAsync(async (req, res) => {
  const last7days = await reportService.getFoodsNumberPerWeek(7);
  const previousWeek = await reportService.getFoodsNumberPerWeek(14, 7);
  const calorieAveragePerUser = await reportService.getCalorieAveragePerUser();
  const total = await foodService.getFoods(isAdmin(req.user.role), req.user);
  res.send({ last7days, previousWeek, calorieAveragePerUser, total: total.length });
});

module.exports = {
  getReports,
};
