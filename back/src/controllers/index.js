module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.foodController = require('./food.controller');
module.exports.mealController = require('./meal.controller');
module.exports.reportController = require('./report.controller');
