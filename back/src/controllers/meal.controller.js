const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { mealService } = require('../services');

const createMeal = catchAsync(async (req, res) => {
  const meal = await mealService.createMeal(req.body);
  res.status(httpStatus.CREATED).send(meal);
});

const getMeals = catchAsync(async (req, res) => {
  const result = await mealService.getMeals();
  res.send(result);
});

const updateMeal = catchAsync(async (req, res) => {
  const meal = await mealService.updateMealById(req.params.mealId, req.body);
  res.send(meal);
});

const deleteMeal = catchAsync(async (req, res) => {
  await mealService.deleteMealById(req.params.mealId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createMeal,
  getMeals,
  updateMeal,
  deleteMeal,
};
