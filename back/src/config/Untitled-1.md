# 👇️ your domain below, e.g. http://localhost:3000
Access-Control-Allow-Origin: http://example.com

Access-Control-Allow-Credentials: true

Access-Control-Allow-Methods: POST, PUT, PATCH, GET, DELETE, OPTIONS

