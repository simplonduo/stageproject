const allRoles = {
  user: ['reporting'],
  admin: ['getUsers', 'manageUsers', 'reporting'],
};

const roles = Object.keys(allRoles);
const roleRights = new Map(Object.entries(allRoles));

const isAdmin = (role) => {
  return role === roles[1];
};

module.exports = {
  roles,
  roleRights,
  isAdmin,
};
