const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createMeal = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    limit: Joi.number().required(),
  }),
};

const updateMeal = {
  params: Joi.object().keys({
    mealId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      limit: Joi.number(),
    })
    .min(1),
};

const deleteMeal = {
  params: Joi.object().keys({
    mealId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createMeal,
  updateMeal,
  deleteMeal,
};
