const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createFood = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    date: Joi.date().required(),
    calorie: Joi.number().required(),
    meal: Joi.required().custom(objectId),
  }),
};

const updateFood = {
  params: Joi.object().keys({
    foodId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      date: Joi.date(),
      calorie: Joi.number(),
      userId: Joi.string(),
      meal: Joi.string(),
    })
    .min(1),
};

const deleteFood = {
  params: Joi.object().keys({
    foodId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createFood,
  updateFood,
  deleteFood,
};
