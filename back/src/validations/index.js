module.exports.authValidation = require('./auth.validation');
module.exports.userValidation = require('./user.validation');
module.exports.foodValidation = require('./food.validation');
module.exports.mealValidation = require('./meal.validation');
